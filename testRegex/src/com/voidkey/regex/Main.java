package com.voidkey.regex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

	static String expReg="^(.{1})(\\d{2})$";
	
	public static void main(String[] args) {
		
		//Pruebas Individuales
		String test="a2a";
		
		//Pruebas por archivo
		
		
		Pattern patt = Pattern.compile(expReg);
		
		File archivo = new File("C:\\test\\test.txt");
		
		try {
			
			BufferedReader lector = new BufferedReader(new FileReader(archivo));
			String linea;
			
			while((linea = lector.readLine())!=null)
			{
				Matcher m = patt.matcher(linea);
				
				while(m.find())
				{
					System.out.println("Linea "+m.group(1)+" - "+m.group(2));
				}
			}
			
		}catch(Exception e)
		{
			System.out.println("Error "+e);
		}
		
		
		//System.out.println(Pattern.compile(expReg).matcher(test).find());

	}
	


}
